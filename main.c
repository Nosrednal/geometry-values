
#include <stdio.h>
#include <stdlib.h>

const double PI = 3.14159;

double detCirculo(double raio){
    return PI*pow(raio, 2);

}
double detCilindro(double raio, double altura){
    if(altura <= 0){
        return 0;
    }
    return PI*raio*raio+detCilindro(raio,altura-1);
}
double detCubo(double raio, double lado, double area){
    area = 6 * (pow(lado, 2));
    raio = pow(lado, 3);
}
double detPiramede(double area, double altura){

    return area * altura / 3;


}
void redirecionadorDeCalculo(int op){
    double aux = 0;
    double raio = 0;
    double altura = 0;
    double lado = 0;
    double area = 0;

    switch(op){
        //1-circulo
        case 1 :
            printf("informe o raio do circulo: ");
            scanf("%lf", &raio);
            aux = detCirculo(raio);
            printf("a area do circulo de raio %.2f eh: %.2f", raio, aux);
        break;

        //2-cilindro
        case 2 :
            printf("informe o raio do cilindro: ");
            scanf("%lf", &raio);
            printf("informe a altura do cilindro: ");
            scanf("%lf", &altura);
            aux = detCilindro(raio, altura);
            printf("a area do cilindro de raio %.2f eh: %.2f", raio, aux, altura);
        break;

        //3-cubo
        case 3 :
            printf("informe o raio do cubo; ");
            scanf("%lf", &lado);
            aux = detCubo(raio, lado, area);
            printf("a area do cubo de raio %.2f eh: %.2f", raio, aux, lado, area);
        break;

        //4-piramede
        case 4 :
            printf("informe a area da piramede: ");
            scanf("%lf", &area);
            printf("informe a altura da piramede: ");
            scanf("%lf", &altura);
            aux = detPiramede(area, altura);
            printf("a area da piramede de raio %.2f eh: %.2f", area, aux, altura);
        break;

        default:
            printf("invalido");
    }
}

int main()
{

    int op;
    printf("qual forma deseja detalhar?\n");
    printf("1-circulo 2-cilindro 3-cubo 4-piramede\n");
    scanf("%d", &op);

    redirecionadorDeCalculo(op);

    return 0;
}
